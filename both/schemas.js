Schema = {};

Schema.UserCountry = new SimpleSchema({
    name: {
        type: String
    },
    code: {
        type: String,
        regEx: /^[A-Z]{2}$/
    }
});

Schema.UserProfile = new SimpleSchema({
    firstName: {
        type: String,
        optional: true
    },
    lastName: {
        type: String,
        optional: true
    },
    birthday: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['Male', 'Female'],
        optional: true
    },
    organisation : {
        type: String,
        optional: true
    },
    website: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: true
    },
    bio: {
        type: String,
        optional: true
    },
    country: {
        type: Schema.UserCountry,
        optional: true
    },
    teamId: {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
      optional: true
    }
});

Schema.User = new SimpleSchema({
    username: {
        type: String,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    emails: {
        type: Array,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: Schema.UserProfile,
        optional: true
    },
    // Make sure this services field is in your schema if you're using any of the accounts packages
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    roles: {
        type: Array,
        optional: true
    },
    'roles.$': {
        type: String
    },
    // In order to avoid an 'Exception in setInterval callback' from Meteor
    heartbeat: {
        type: Date,
        optional: true
    }
});

Meteor.users.attachSchema(Schema.User);

/******************************************/
/***********ADDITIONAL SCHEMAS*************/
/******************************************/

Lists = new Mongo.Collection('lists');

// Deny all client-side updates since we will be using methods to manage this collection
Lists.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Schema.Lists = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  name: {
    type: String,
    optional: true
  },
  teamId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    index: 1,
    optional: true //TODO: Optional for now - make it required in future enhancements
  },
  creatorId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    denyUpdate: true,
    index: 1,
    optional: true
  },
  createdAt: {
    type: Date,
    denyUpdate: true
  },
  modified: {
    type: Date
  }
});

Lists.attachSchema(Schema.Lists);

ListItems = new Mongo.Collection('list_items');

// Deny all client-side updates since we will be using methods to manage this collection
ListItems.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

Schema.ListItems = new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
  },
  name: {
    type: String,
    index: 1
  },
  description: {
    type: String
  },
  teamId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    index: 1,
    optional: true //TODO: Optional for now - make it required in future enhancements
  },
  creatorId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    denyUpdate: true,
    index: 1
  },
  listId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    index: 1
  },
  thumbsUp: {
    type: Number,
    defaultValue: 0
  },
  thumbsDown: {
    type: Number,
    defaultValue: 0
  },
  stars: {
    type: Number,
    defaultValue: 0
  },
  lastCommit: {
    type: Date
  },
  githubLink: {
    type: String
  },
  comment: {
    type: String,
    optional: true
  },
  language: {
    type: String
  },
  createdAt: {
    type: Date,
    denyUpdate: true
  },
  modified: {
    type: Date
  }
});

ListItems.attachSchema(Schema.ListItems);

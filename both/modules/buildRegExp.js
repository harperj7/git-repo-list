let buildRegExp = (searchText) => {
  // this is a dumb implementation
  let parts = searchText.trim().split(/[ \-\:]+/);

  return new RegExp("(" + parts.join('|') + "|" + searchText + ")", "ig");

};

Modules.buildRegExp = buildRegExp;

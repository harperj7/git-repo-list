Routes = {
  redirects: {
    authenticatedRedirect() {
      if (!Meteor.loggingIn() && !Meteor.userId()) {
        FlowRouter.go('login');
      }
    },
    publicRedirect() {
      if (Meteor.userId()) {
        FlowRouter.go('dashboard');
      }
    }
  }
}

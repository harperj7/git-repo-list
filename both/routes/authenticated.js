const authenticatedRoutes = FlowRouter.group({
  name: 'authenticated',
  triggersEnter: [Routes.redirects.authenticatedRedirect]
});

authenticatedRoutes.route('/dashboard', {
    name: 'dashboard',
    action: function() {
        BlazeLayout.render("layout", {content: "dashboard"});
    }
});

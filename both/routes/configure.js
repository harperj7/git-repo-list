// FlowRouter.notFound = {
//   action() {
//     BlazeLayout.render( 'default', { yield: 'notFound' } );
//   }
// };
FlowRouter.notFound = {
    action: function() {
        BlazeLayout.render('error');
    }
};

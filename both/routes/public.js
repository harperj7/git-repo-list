const publicRoutes = FlowRouter.group({
  name: 'public',
  triggersEnter: [Routes.redirects.publicRedirect]
});
//
// publicRoutes.route( '/signup', {
//   name: 'signup',
//   action() {
//     BlazeLayout.render( 'default', { yield: 'signup' } );
//   }
// });
//
// publicRoutes.route( '/login', {
//   name: 'login',
//   action() {
//     BlazeLayout.render( 'default', { yield: 'login' } );
//   }
// });
//
// publicRoutes.route( '/recover-password', {
//   name: 'recover-password',
//   action() {
//     BlazeLayout.render( 'default', { yield: 'recoverPassword' } );
//   }
// });
//
// publicRoutes.route( '/reset-password/:token', {
//   name: 'reset-password',
//   action() {
//     BlazeLayout.render( 'default', { yield: 'resetPassword' } );
//   }
// });
// publicRoutes.route('/', {
//     action: function() {
//         FlowRouter.go('/dashboard');
//     }
// });



publicRoutes.route('/', {
    action: function() {
        FlowRouter.go('/dashboard');
    }
});


publicRoutes.route('/login', {
    name: 'login',
    action: function() {
        BlazeLayout.render("blankLayout", {content: "login"});
    }
});

publicRoutes.route('/register', {
    name: 'register',
    action: function() {
        BlazeLayout.render("blankLayout", {content: "register"});
    }
});

publicRoutes.route('/forgotPassword', {
    name: 'forgotPassword',
    action: function() {
        BlazeLayout.render("blankLayout", {content: "forgotPassword"});
    }
});

FlowRouter.route('/error', {
    name: 'error',
    action: function() {
        BlazeLayout.render("blankLayout", {content: "error"});
    }
});

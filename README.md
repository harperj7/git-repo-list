# Git Repo List README

This application was built using the [MeteorJS platform](https://github.com/meteor/meteor).

##The Brief

Allow the users to produce a list of Github repos that might be useful for the team.  The user should be able to search Github from within the app including specifying the language.  The user should be able to select the repo and add a comment for the team.  Once selected, other team members should be able to see the list (including the original comment) and give it a thumb up or down.  Users cannot vote for their own selection nor can they add a repo that is already in the list. The list of selected repos should be sorted by the thumb difference first (thumbs up - thumbs down), if they are equal then total thumbs up, if they are equal total github stars, if that is equal then just alphabetically.  Users should also be able to filter the list based on the language.

###Requirements summary

+ Users need to be able to create a list of repos that might be useful for the team.
+ Be able to search public repos on Github from the app and specify the language.
+ Select a repo from the search results to add to the list and add a comment.
+ Other team members can see the list of repos and their respective comments.
+ Team members can vote each item in the list up or down.
+ Users can't vote for repos that they have added to the list.
+ Users can't add repos that already exist in the list.
+ The list can be filtered by language.
+ The list of repos is sorted by:
    1. Thumbs up and down difference (score)
    2. Total thumbs up
    3. Total Github stars
    4. Alphabetically

------

###Version 1

+ Search for public repos on Github via the app.
+ Add repos to the default list.
+ Delete repos from the list that they added (but not ones that someone else added).
+ Vote for repos in the list that have been added by other people.

------

###Version 2

+ Clear the Github search results.
+ Narrow the search by language.
+ Filter the list of repos by language.
+ Clear the filter.
+ Add a comment to the item they add to the list.
+ Prevented from being able to vote on an item more than once.
+ Prevented from adding an item that has already been added to the list.
+ The delete button for each item in the list is only shown to the user that added that item.
+ A new collection has been added for keeping track of votes cast.

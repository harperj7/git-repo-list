let login = (options) => {
  _validate(options.form, options.template);
};

let _validate = (form, template) => {
  $(form).validate(validation(template));
};

let validation = (template) => {
  return {
    rules: {
      loginUser: {
        required: true
      },
      password: {
        required: true
      }
    },
    messages: {
      emailAddress: {
        required: 'Please enter your email address.'
      },
      password: {
        required: 'Please enter your password.'
      }
    },
    submitHandler() {
      _handleLogin(template);
    }
  };
};

let _handleLogin = (template) => {

  let loginUser = template.find('[name="loginUser"]').value,
    password = template.find('[name="password"]').value;

  Meteor.loginWithPassword(loginUser, password, (error) => {
    if (error) {
      alertify.error(error.reason);
    } else {
      alertify.success('Logged in!');
      FlowRouter.go('dashboard');
    }
  });
};

Modules.client.login = login;

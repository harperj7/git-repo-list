let signup = (options) => {
  _validate(options.form, options.template);
};

let _validate = (form, template) => {
  $(form).validate(validation(template));
};

let validation = (template) => {
  return {
    rules: {
      emailAddress: {
        required: true,
        email: true
      },
      username: {
        required: true
      },
      password: {
        required: true
      }
    },
    messages: {
      emailAddress: {
        required: 'An email address is required.',
        email: 'Please enter a valid email address.'
      },
      username: {
        required: 'Please enter a username.'
      },
      password: {
        required: 'Please enter a password.'
      }
    },
    submitHandler() {
      _handleSignup(template);
    }
  };
};

let _handleSignup = (template) => {

  let user = {
    email: template.find('[name="emailAddress"]').value,
    password: template.find('[name="password"]').value,
    username: template.find('[name="username"]').value,
    profile: {
      firstName: template.find('[name="firstName"]').value,
      lastName: template.find('[name="lastName"]').value
    }
  };

  console.log('user:', user);

  Meteor.call('createCustomer', user, function(error, response) {
    console.log('error:', error, 'response:', response);
    if (error) {
      console.log('create customer error:');
    } else {
      console.log('create customer no error');

      Meteor.loginWithPassword(user.email, user.password, function(error) {
        if (error) {
          console.log('login error', error);
        } else {
          FlowRouter.go('dashboard');
          alertify.success('Your account has been successfully created, welcome!');
          // Meteor.call('sendVerificationLink', Meteor.userId(), (error, response) => { TODO: send an email verification link to the user
          //   if (error) {
          //     Bert.alert(error.reason, 'danger');
          //   } else {
          //     Bert.alert('Your account has successfully been created and you are now logged in! An email verification link has also been sent to you.', 'success');
          //   }
          // });
        }
      });
    }
  });
}

Modules.client.signup = signup;

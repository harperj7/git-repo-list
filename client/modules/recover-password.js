let recoverPassword = (options) => {
  _validate(options.form, options.template);
};

let _validate = (form, template) => {
  $(form).validate(validation(template));
};

let validation = (template) => {
  return {
    rules: {
      emailAddress: {
        required: true,
        email: true
      }
    },
    messages: {
      emailAddress: {
        required: 'Please enter your email address.',
        email: 'Please make sure the email address you entered is valid.'
      }
    },
    submitHandler() {
      _handleRecovery(template);
    }
  };
};

let _handleRecovery = (template) => {
  let email = template.find('[name="emailAddress"]').value;

  Accounts.forgotPassword({
    email: email
  }, (error) => {
    if (error) {
      alertify.warning(error.reason);
    } else {
      alertify.success('Check your email inbox for a password reset link!');
    }
  });
};

Modules.client.recoverPassword = recoverPassword;

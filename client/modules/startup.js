let startup = () => {
  alertify.defaults = {
    autoReset: true,
    basic: false,
    closable: true,
    closableByDimmer: true,
    frameless: false,
    maintainFocus: true, // <== global default not per instance, applies to all dialogs
    maximizable: false,
    modal: true,
    movable: false,
    moveBounded: false,
    overflow: true,
    padding: true,
    pinnable: true,
    pinned: true,
    preventBodyShift: false, // <== global default not per instance, applies to all dialogs
    resizable: false,
    startMaximized: false,
    transition: 'zoom',

    // notifier defaults
    notifier: {
      // auto-dismiss wait time (in seconds)
      delay: 5,
      // default position
      position: 'top-right'
    },

    // language resources
    glossary: {
      // dialogs default title
      title: 'Alert',
      // ok button text
      ok: 'Okay',
      // cancel button text
      cancel: 'Cancel'
    },

    // theme settings
    theme: {
      // class name attached to prompt dialog input textbox.
      input: 'ajs-input',
      // class name attached to ok button
      ok: 'ajs-ok',
      // class name attached to cancel button
      cancel: 'ajs-cancel'
    }
  };

};

Modules.client.startup = startup;

let resetPassword = (options) => {
  _validate(options.form, options.template);
};

let _validate = (form, template) => {
  $(form).validate(validation(template));
};

let validation = (template) => {
  return {
    rules: {
      newPassword: {
        required: true,
        minlength: 6
      },
      repeatNewPassword: {
        required: true,
        minlength: 6,
        equalTo: '[name="newPassword"]'
      }
    },
    messages: {
      newPassword: {
        required: "Please enter a new password.",
        minlength: "Please use at least six characters."
      },
      repeatNewPassword: {
        required: "Please enter your new password again.",
        equalTo: "Your passwords don't match, please try again."
      }
    },
    submitHandler() {
      _handleReset(template);
    }
  };
};

let _handleReset = (template) => {
  var token = FlowRouter.current().params.token,
    password = template.find('[name="newPassword"]').value;

  Accounts.resetPassword(token, password, (error) => {
    if (error) {
      alertify.error(error.reason);
    } else {
      alertify.success('Your password has successfully been reset!');
    }
  });
};

Modules.client.resetPassword = resetPassword;

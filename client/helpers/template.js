Template.registerHelper('getState', (key) => {
  return (typeof Template.instance().state !== "undefined")
    ? Template.instance().state.get(key)
    : null;
});

Template.registerHelper('equals', (value1, value2) => {
  return (value1 === value2);
});

Template.registerHelper('selected', (value1, value2) => {
  return (value1 === value2) ? 'selected' : '';
});

Template.registerHelper('checked', (value) => {
  return (value) ? 'checked' : '';
});

Template.registerHelper('formatDate', (date) => {
  return moment(date).format("MMM Do YYYY");
});

Template.registerHelper('difference', (num1, num2) => {
  return num1 - num2;
});

Template.registerHelper('getUsername', (userId) => {
  const user = Meteor.users.findOne(userId);
  return (user) ? user.username : 'Unknown';
});

// import Fuse from 'fuse.js';

Template.header.onCreated(function() {
  const template = this;

  template.state = new ReactiveDict();
  template.state.setDefault({loading: false, searchTerm: null});
  template.data = {};

  template.autorun(function() {

    const currentPage = FlowRouter.getRouteName();

    if (currentPage) {

      template.state.set('loading', true);
      if (template.state.get('searchTerm')) {
        template.subscribe(currentPage + 'Search', {searchText: template.state.get('searchTerm')}, function() {
          template.state.set('loading', false);
        });
      } else {
        template.subscribe(currentPage, function() {
          template.state.set('loading', false);
        });
      }

    }
  });

  template.autorun(function() {
    let currentPage = FlowRouter.getRouteName(),
      collection = currentPage.charAt(0).toUpperCase() + currentPage.slice(1);

    if (typeof global[collection] !== 'undefined') {
      let currentData = global[collection].find().fetch();

      template.data.pageData = currentData;
    }
  });

});

Template.header.events({

  // Toggle left navigation
  'click .left-nav-toggle a': function(event) {
    event.preventDefault();
    event.stopPropagation();

    $("body").toggleClass("nav-toggle");
  },
  'click .logout': function(event, template) {
    event.preventDefault();
    event.stopPropagation();

    Meteor.logout((error) => {
      if (error) {
        alertify.error('Error logging out: ' + error.reason);
      } else {
        alertify.success('Logged out!');
        FlowRouter.go('login');
      }
    });
  },
  'keyup .search-data': function(event, template) {

    let temp = Template.instance();
    let searchText = $(event.target).val().trim();

    template.state.set('searchTerm', searchText);

  }

});

Template.header.helpers({
  getUser: function() {
    if (typeof Meteor.user() !== 'undefined') return Meteor.user().username ? Meteor.user().username : Meteor.user().emails[0].address;
  }
});

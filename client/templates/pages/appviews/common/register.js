Template.register.onRendered( () => {
  Modules.client.signup({
    form: "#register",
    template: Template.instance()
  });
});

Template.register.events({
  'submit form': ( event ) => event.preventDefault()
});

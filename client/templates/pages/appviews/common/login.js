Template.login.onCreated(function() {
  console.log('this template: ', this);
});

Template.login.onRendered(() => {

  Modules.client.login({
    form: "#login",
    template: Template.instance()
  });

});

Template.login.events({
  'submit form': function(event, template) {

    event.preventDefault();

console.log('submitted login form');
    var loginButton = $('.loginButton').button('loading');

    let data = {};
    data.loginUser = template.find('[name="loginUser"]').value;
    data.password = template.find('[name="password"]').value;

    console.log('data:', data);

    Meteor.call('checkUserLogin', data, function(err, response) {

      console.log('err:', err, 'response', response);

      if (!err) {

        if (response !== false) {
          console.log('cake');
          Meteor.loginWithPassword(data.loginUser, data.password, function(error) {
            if (error) {
              console.log('login error', error);
            } else {
              FlowRouter.go('dashboard');
              alertify.success('Logged in');
            }
          });
        } else {
          alertify.error('Sausages!');
          loginButton.button('reset');
        }
      } else {
        // Modules.Errors.throwError('Reactivate account login - checkUserLogin failed', err, null, true);
        loginButton.button('reset');
      }
    });

  }
});

Template.dashboard.onCreated(function() {

  const template = this;

  template.state = new ReactiveDict();
  template.state.setDefault({loading: true, filtering: false, githubSearchLoading: false});
  template.data.githubSearchResults = new ReactiveArray();

  template.autorun(() => {

    template.state.set('loading', true);

    template.subscribe('listItems', function() {

      template.state.set('loading', false);

    });

  });

});

Template.dashboard.events({
  "submit #searchGithub": function(e, tmpl) {

    e.preventDefault();
    e.stopPropagation();

    tmpl.state.set('githubSearchLoading', true);

    let query = e.currentTarget.search.value;

    if (query) {

      Meteor.call("github.searchRepos", {
        query: query
      }, function(error, result) {
        if (error) {
          console.log("error", error);
          alertify.notify(error.reason, 'error', 3);
        }
        if (result) {
          console.log(result);
          if (result.data.items.length > 0) {
            tmpl.data.githubSearchResults.set(result.data.items);
          }
        }
        tmpl.state.set('githubSearchLoading', false);
      });

    } else {
      alertify.notify('Please enter a search term before submitting', 'warning', 2);
    }
  },
  "click .add-repo": function(e, tmpl) {

    e.preventDefault();
    e.stopPropagation();

    let index = $(e.currentTarget).closest('.search-result').data('id'),
        githubData = tmpl.data.githubSearchResults.get(index);

    Meteor.call("ListItems.insert", githubData, function(error, result) {
      if (error) {
        console.log("error", error);
      }
      if (result) {
        console.log(result);

      }
      tmpl.state.set('githubSearchLoading', false);
    });

  },
  'click .delete': function(e, tmpl) {

    e.preventDefault();
    e.stopPropagation();

    const listItemId = $(e.currentTarget).closest('.item-row').data('id');

    if (listItemId) {
      Meteor.call("ListItems.delete", listItemId, function(error, result) {
        if (error) {
          alertify.notify(error.reason, 'error', 3);
        }
        if (result) {
          alertify.notify('Successfully removed the specified list item.', 'success', 3);
        }
      });
    }
  },
  'click .thumbs-up': function(e, tmpl) {

    e.preventDefault();
    e.stopPropagation();

    const listItemId = $(e.currentTarget).closest('.item-row').data('id');

    if (listItemId) {
      Meteor.call("ListItems.vote", {listId: listItemId, thumbsUp: 1}, function(error, result) {
        if (error) {
          alertify.notify(error.reason, 'error', 3);
        }
        if (result) {
        }
      });
    }
  },
  'click .thumbs-down': function(e, tmpl) {

    e.preventDefault();
    e.stopPropagation();

    const listItemId = $(e.currentTarget).closest('.item-row').data('id');

    if (listItemId) {
      Meteor.call("ListItems.vote", {listId: listItemId, thumbsDown: 1}, function(error, result) {
        if (error) {
          alertify.notify(error.reason, 'error', 3);
        }
        if (result) {
        }
      });
    }
  }
});

Template.dashboard.helpers({
  githubSearchResults: function() {
    return Template.instance().data.githubSearchResults.get();
  },
  listItems: function() {
    return ListItems.find();
  }

});

Template.myModal.onCreated(function() {

  const template = this;

  template.state = new ReactiveDict();
  template.state.setDefault({loading: true});

  template.autorun(function() {
    // template.subscribe('collection');

    if (template.subscriptionsReady()) {
      template.state.set('loading', false);
    }
  });

});

Template.myModal.helpers({

});

Template.myModal.events({
  'submit #myForm': function(e, template) {

    e.preventDefault();
    e.stopPropagation();


  }
});

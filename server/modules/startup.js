const _setEnvironmentVariables = () => Modules.server.setEnvironmentVariables(),
  _ensureIndexes = () => Modules.server.ensureIndexes(),
  _setBrowserPolicies = () => {},
  _generateAccounts = () => Modules.server.generateAccounts();

const startup = function() {
  _setEnvironmentVariables();
  _setBrowserPolicies();
  _ensureIndexes();
  // _generateAccounts();
  (function() {
    let listExists = Lists.findOne();
    if (!listExists) Lists.insert({name: 'Default list', createdAt: new Date(), modified: new Date()})
  })();
};

Modules.server.startup = startup;

Meteor.publish('listItems', function() {

  const userId = this.userId;

  if (userId) {

    return ListItems.find();

  }

  return null;

});

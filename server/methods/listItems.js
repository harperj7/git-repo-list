Meteor.methods({
  'ListItems.insert': function(data) {

    check(data, Object);

    let result = ListItems.insert({
      name: data.name,
      description: data.description,
      creatorId: Meteor.userId(),
      listId: Lists.findOne()._id,
      lastCommit: data.pushed_at,
      githubLink: data.html_url,
      language: data.language,
      stars: data.stargazers_count,
      createdAt: new Date(),
      modified: new Date()
    });

    return result;

  },
  'ListItems.delete': function(id) {

      check(id, String);

      const listItem = ListItems.findOne(id);

      if (listItem) {
        if (listItem.creatorId === Meteor.userId()) {
          return ListItems.remove(id);
        } else {
          throw new Meteor.Error('ListItems.delete', 'Cannot delete this list item. Permission denied.');
        }
      } else {
        throw new Meteor.Error('ListItems.delete', 'Unable to find that list item.');
      }
  },
  'ListItems.vote': function(data) {

      check(data, Object);

      const listItem = ListItems.findOne(data.listId);

      if (listItem) {
        if (listItem.creatorId !== Meteor.userId()) {
          if (typeof data.thumbsUp !== 'undefined') {
            let currentThumbsUp = listItem.thumbsUp;
            ListItems.update(data.listId, {
              $set: {
                thumbsUp: currentThumbsUp + 1
              }
            });
          } else if (typeof data.thumbsDown !== 'undefined') {
            let currentThumbsDown = listItem.thumbsDown;
            ListItems.update(data.listId, {
              $set: {
                thumbsDown: currentThumbsDown + 1
              }
            });
          }
        } else {
          throw new Meteor.Error('ListItems.delete', 'You cannot vote for an item that you added to the list.');
        }
      } else {
        throw new Meteor.Error('ListItems.delete', 'Unable to find that list item.');
      }
  }

});

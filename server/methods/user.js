Meteor.methods({

  createCustomer: function(customer) {

    check(customer, {
      username: String,
      email: String,
      password: String,
      profile: Object
    });

    var emailRegex = new RegExp(customer.email, "i");

    var lookupCustomer = Meteor.users.findOne({"emails.address": emailRegex});

    if (!lookupCustomer) {
      try {
        var user = Accounts.createUser({username: customer.username, email: customer.email, password: customer.password, profile: customer.profile, roles: []});

        Roles.addUsersToRoles(user, ['user']);

        return user;

      } catch (exception) {
        return exception;
      }
    } else {
      return 'User already exists';
    }

  },
  checkUserLogin(data) {

    check(data, {
      loginUser: String,
      password: String
    });

    try {
      if (ApiPassword.validate({email: data.loginUser, password: data.password})) {
        let user = Accounts.findUserByEmail(data.loginUser);
        let result = {
          userId: user._id
        };

        return result;

      }

      return false;

    } catch (ex) {
      throw new Meteor.Error(500, ex.message);
    }
  }
});

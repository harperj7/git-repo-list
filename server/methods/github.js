const GitHubApi = require("github"),
  github = new GitHubApi({
    debug: Meteor.settings.private.github.debug,
    protocol: "https",
    host: "api.github.com",
    pathPrefix: "",
    headers: {
      "user-agent": "git-repo-list @harperj7"
    },
    Promise: require('bluebird'),
    followRedirects: false,
    timeout: 5000
  }),
  Future = Npm.require('fibers/future'),
  Fiber = Npm.require('fibers');

Meteor.methods({
  'github.searchRepos': function(data) {

    check(data, Object);

    let searchReposResult = new Future();

    if (typeof data.query !== 'undefined' && data.query.trim()) {

      github.authenticate({type: "token", token: Meteor.settings.private.github.token});
      github.search.repos({
        q: data.query,
        page: 1,
        per_page: 15,
        sort: 'stars',
        order: 'desc'
      }, function(err, res) {
        if (err) {
          Fiber(function() {
            throw new Meteor.Error('github.searchRepos', err);
          }).run();
        }
        if (res) {
          Fiber(function() {
            // searchReposResult.return (JSON.stringify(res));
            searchReposResult.return (res);
          }).run();

        }
      });
    } else {
      throw new Meteor.Error(500, 'Please enter a search term.');
    }
    return searchReposResult.wait();

  }
});
